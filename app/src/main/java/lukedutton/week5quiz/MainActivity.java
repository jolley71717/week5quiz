package lukedutton.week5quiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;
import java.lang.*;

public class MainActivity extends AppCompatActivity {



    String[][] questionArray = new String[][]{{"Which number is less than 5?", "6", "4", "3", "10"},
            {"What is the capital of Montana", "Helena", "Billings", "Missoula", "Butte"},
            {"What is the average wind-speed of a swallow carrying a coconut?", "I don't know that", "55mph", "African or European?", "Tim"},
            {"What is 6 + 6?", "11", "13", "0", "12"},
            {"What could x be in the equation x^2 = 25?", "6", "5", "15", "-5"},
            {"Why is a mouse when it spins?", "The higher the fewer", "Only in the summer", "I hate riddles", "mist"},
            {"What grade should I get in this course", "A", "B", "C", "D"},
            {"What two programming languages should you be familiar with to program in Android Studio?", "XML", "C#", "Python", "Java"},
            {"What is the 5th color of the rainbow?", "Red", "Blue", "Indigo", "Yellow"},
            {"Which items below are as heavy as 1 pound of feathers?", " 1 pound of grass", "1 pound of lead", "1 pound of aluminum", "1 pound of titanium"}};

    String[][] answerArray = new String[][]
            {{"F", "T", "T", "F"},
                    {"T", "F", "F", "F"},
                    {"F", "F", "T", "F"},
                    {"F", "F", "F", "T"},
                    {"F", "T", "F", "T"},
                    {"T", "F", "F", "F"},
                    {"T", "F", "F", "F"},
                    {"T", "F", "F", "T"},
                    {"F", "T", "F", "F"},
                    {"T", "T", "T", "T"}};
    String[][] userAnsers = new String[10][4];

    TextView tvQuestion;
    TextView tvQuestionNumber;
    CheckBox rbQ1;
    CheckBox rbQ2;
    CheckBox rbQ3;
    CheckBox rbQ4;
    Button btnNextQuestion;
    int currentQuestionIndex = 0;
    Double finalScore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvQuestion = (TextView) findViewById(R.id.tvQuestion);
        tvQuestionNumber = (TextView) findViewById(R.id.tvQuestionNumber);
        rbQ1 = (CheckBox) findViewById(R.id.rbResponse1);
        rbQ2 = (CheckBox) findViewById(R.id.rbResponse2);
        rbQ3 = (CheckBox) findViewById(R.id.rbResponse3);
        rbQ4 = (CheckBox) findViewById(R.id.rbResponse4);
        btnNextQuestion = (Button) findViewById(R.id.btnNextQuestion);

        btnNextQuestion.setVisibility(View.INVISIBLE);
        questionPopulate(currentQuestionIndex);
        tvQuestion.setTextSize(20);


    }

    public void onRadioButtonClicked(View view) {
        displayNext();

//        // Is the button now checked?
//        boolean checked = ((RadioButton) view).isChecked();
//
//        switch(view.getId()) {
//            case R.id.rbResponse1:
//                if (rbQ1)
//                    rbQ1.setChecked(false);
//                    break;
//            case R.id.rbResponse2:
//                if (checked)
//                    rbQ2.setChecked(false);
//                    break;
//            case R.id.rbResponse3:
//                if (checked)
//                    rbQ3.setChecked(false);
//                    break;
//            case R.id.rbResponse4:
//                if (checked)
//                    rbQ4.setChecked(false);
//                    break;
//        }
    }

    public void onNextButton(View view) {


        String rb1Entry;
        String rb2Entry;
        String rb3Entry;
        String rb4Entry;

        if (rbQ1.isChecked()) rb1Entry = "T";
        else rb1Entry = "F";
        if (rbQ2.isChecked()) rb2Entry = "T";
        else rb2Entry = "F";
        if (rbQ3.isChecked()) rb3Entry = "T";
        else rb3Entry = "F";
        if (rbQ4.isChecked()) rb4Entry = "T";
        else rb4Entry = "F";

        userAnsers[currentQuestionIndex][0] = rb1Entry;
        userAnsers[currentQuestionIndex][1] = rb2Entry;
        userAnsers[currentQuestionIndex][2] = rb3Entry;
        userAnsers[currentQuestionIndex][3] = rb4Entry;

        if (currentQuestionIndex < 9) {
            currentQuestionIndex++;
            questionPopulate(currentQuestionIndex);
        } else {
            finalScoreCalculation();
            //go to final score method and change the font of the textbox and make everything else invisible except the reset button, which I'll have to make
            tvQuestion.setTextSize(40);
            tvQuestion.setText("Your Final score is: \n" + Double.toString(finalScore) + "%");

            rbQ1.setVisibility(View.INVISIBLE);
            rbQ2.setVisibility(View.INVISIBLE);
            rbQ3.setVisibility(View.INVISIBLE);
            rbQ4.setVisibility(View.INVISIBLE);
            btnNextQuestion.setVisibility(View.INVISIBLE);

        }
    }

    public void questionPopulate(int question) {

        String visibleQuestionIndex = "Question #" + Integer.toString(question + 1);

        tvQuestionNumber.setText(visibleQuestionIndex);
        tvQuestion.setText(questionArray[question][0]);
        rbQ1.setText(questionArray[question][1]);
        rbQ2.setText(questionArray[question][2]);
        rbQ3.setText(questionArray[question][3]);
        rbQ4.setText(questionArray[question][4]);


        questionReset();
        displayNext();


    }

    public void questionReset() {
        rbQ1.setChecked(false);
        rbQ2.setChecked(false);
        rbQ3.setChecked(false);
        rbQ4.setChecked(false);
    }

    public void finalScoreCalculation() {
        int totalCorrect = 0;

        for (int i = 0; i < 10; i++) {
            Boolean isCorrect = false;
            Boolean testQuestion[] = new Boolean[4];

            for (int t = 0; t < 4; t++) {
                if (userAnsers[i][t].equals(answerArray[i][t])) {
                    testQuestion[t] = true;
                } else testQuestion[t] = false;
            }
            isCorrect = isAllTrue(testQuestion);

            if (isCorrect) totalCorrect++;
        }
        String stringTotal = Integer.toString(totalCorrect);
        Double calculatedScore = Double.parseDouble(stringTotal) / 10.00;
        finalScore = calculatedScore * 100.00;

    }

    public static boolean isAllFalse(Boolean[] array) {
        for (Boolean b : array) if (b) return false;
        return true;
    }

    public static boolean isAllTrue(Boolean[] array) {
        for (Boolean b : array) if (!b) return false;
        return true;
    }


    public void onQuizReset(View view) {
        currentQuestionIndex = 0;
        tvQuestion.setTextSize(20);
        questionPopulate(currentQuestionIndex);
        rbQ1.setVisibility(View.VISIBLE);
        rbQ2.setVisibility(View.VISIBLE);
        rbQ3.setVisibility(View.VISIBLE);
        rbQ4.setVisibility(View.VISIBLE);

    }

    public void displayNext() {
        Boolean[] rbQChecked = new Boolean[]{rbQ1.isChecked(), rbQ2.isChecked(), rbQ3.isChecked(), rbQ4.isChecked()};

        if (isAllFalse(rbQChecked)) {
            btnNextQuestion.setVisibility(View.INVISIBLE);
        } else btnNextQuestion.setVisibility(View.VISIBLE);
    }
}
